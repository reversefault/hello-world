
# Run Locally
./mvnw spring-boot:run

# Docker Build
mvn clean install
docker build -t hello-world:1.0 .
docker run --name hello-world-app --rm -p 8080:8080 hello-world:1.0
Browse to http://localhost:8080
docker stop hello-world-app


# Upload to Docker
docker login
docker tag actuator-sample:1.0 johnhills/hello-world:1.0
docker push johnhills/hello-world:1.0

