
# Establish upstream repo
git remote add upstream git@gitlab.com:reversefault/hello-world.git

# See remotes
git remote -v
origin	https://gitlab.com/reversefault/hello-world-fork.git (fetch)
origin	https://gitlab.com/reversefault/hello-world-fork.git (push)
upstream	git@gitlab.com:reversefault/hello-world.git (fetch)
upstream	git@gitlab.com:reversefault/hello-world.git (push)

# Pull from upstream
git fetch upstream
remote: Enumerating objects: 29, done.
remote: Counting objects: 100% (29/29), done.
remote: Compressing objects: 100% (19/19), done.
remote: Total 29 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (29/29), 63.61 KiB | 15.90 MiB/s, done.
From gitlab.com:reversefault/hello-world
 * [new branch]      main       -> upstream/main


git checkout main
git merge upstream/main

